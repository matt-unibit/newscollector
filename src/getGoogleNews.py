import requests
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize
import json
import os
import math
import getpass
import datetime
import dateutil.relativedelta

#difference in concat call is due to un-updated version of pandas on server
def mergeDFs(df1, df2):
    if getpass.getuser() == 'Unibit':
        output = pd.concat([df1, df2], sort=True)
    if getpass.getuser() == 'data':
        output = pd.concat([df1, df2])
    return output

def run():
    #define directories based on Matt's computer(for testing) vs Server(for production)
    if getpass.getuser() == 'Unibit':
        repositoryPath = '/Users/Unibit/PycharmProjects/newscollector/'
        outputPath = '/Users/Unibit/PycharmProjects/newscollector/data/GoogleNews/'
    if getpass.getuser() == 'data':
        repositoryPath = '/home/data/Updated_Files/src/newscollector/'
        outputPath = '/home/data/unibit-filesystem/Stock/Events/GoogleNews/'


    namesTickers = pd.read_csv(repositoryPath + 'data/companies.csv', encoding='utf-8')
    names = namesTickers['name'].tolist()
    files = os.listdir(outputPath)
    #constant API call parameters
    key = 'd9545c393e6f4529894a94ee6014cb68'  # Vincent@Unibit.biz google news API account key
    sources = 'financial-post,bloomberg,business-insider,reuters,the-wall-street-journal'
    toDate = datetime.date.today().isoformat()
    fails = []

    for name in names:
        print(name)
        t = namesTickers.loc[namesTickers['name'] == name, 'symbol'].to_string(index=False).split('?')[0]
        if str(t) + '.csv' in files:
            df = pd.read_csv(outputPath + str(t) + '.csv', encoding='utf-8')
            fromDate = df.publishedAt.max()[0:10]
        else:
            df = pd.DataFrame()
            fromDate = (datetime.date.today() + dateutil.relativedelta.relativedelta(months=-6, days=1)).isoformat()
        endpoint = 'https://newsapi.org/v2/everything?q=' + str(name).encode(encoding="utf-8") + '&language=en&pageSize=100&apiKey=' + \
                   key + '&sources=' + sources + '&from=' + str(fromDate) + '&to=' + str(toDate)

        data = json.loads(requests.get(endpoint).text)
        if data['status'] == 'error':
            continue
        Tpages = int(math.ceil(data['totalResults'] / 100))
        dfTemp = json_normalize(data['articles'])
        df = mergeDFs(df, dfTemp)
        page = 1
        while page <= Tpages:
            print(str(page) + " / " +str(Tpages))
            page += 1
            endpoint = 'https://newsapi.org/v2/everything?q=' + str(name).encode(encoding="utf-8") + '&language=en&pageSize=100&apiKey=' + \
                       key + '&page=' + str(page) + '&sources=' + sources + '&from=' + str(fromDate) + '&to=' + \
                       str(toDate)
            data = json.loads(requests.get(endpoint).text)
            if data['status'] == 'error':
                continue
            dfTemp = json_normalize(data['articles'])
            df = mergeDFs(df, dfTemp)
        try:
            df.drop_duplicates(subset='title', inplace=True)
            df = df.sort_values(by=['publishedAt'], ascending=False)
            df.to_csv(outputPath + t + '.csv', encoding='utf-8', index=False)
        except:
            fails.append(t)
    pd.DataFrame(np.array(fails)).to_csv(outputPath + 'FAILURES.csv', encoding='utf-8', index=False)


if __name__ == "__main__":
    run()
