import requests
import pandas as pd
from pandas.io.json import json_normalize
import json
import getpass

if getpass.getuser() == 'Unibit': #Matt's personal computer filepath
    datapath = '/Users/Unibit/PycharmProjects/newscollector/data/IEXNews/'
if getpass.getuser() == 'data': #server filepath
    datapath = '/home/data/unibit-filesystem/Stock/Events/IEX/'

tickers = pd.read_csv(datapath + 'tickers.csv', encoding='utf-8').symbol.tolist()
for i in tickers:
    if getpass.getuser() == 'Unibit':
        print(i)
    try:
        df = pd.read_csv(datapath + i + '.csv', encoding='utf-8')
    except:
        pass
    data = requests.get("https://api.iextrading.com/1.0/stock/" + i + "/news/last/50")
    data = json.loads(data.text, encoding="utf-8")
    if len(data) == 0:
        continue
    for article in data:
        article = json_normalize(article)
        if dir().count('df') == 1:
            if getpass.getuser() == 'Unibit':
                df = pd.concat([df, article], sort=True)
            if getpass.getuser() == 'data':
                df = pd.concat([df, article]) #this code is in place due to un-updated pandas version in server
        else:
            df = article
    df.drop_duplicates(inplace=True)
    df.to_csv(datapath + i + '.csv', encoding='utf-8', index=False)
    del df